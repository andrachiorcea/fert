const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const bodyParser = require('body-parser');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const tokenRouter = require('./routes/token');
const feedbackRouter = require('./routes/feedback');
const questionRouter = require('./routes/question');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, '../front')));
app.use(express.static(path.join(__dirname, '../front/html')));

app.use('', indexRouter);
app.use('/API/users', usersRouter);
app.use('/API/token', tokenRouter);
app.use('/API/question', questionRouter);
app.use('/API/feedback', feedbackRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.json({
      message: err.message,
      error: err
  });});

// websockets
var WebSocketServer = require('websocket').server;
var http = require('http');

var server = http.createServer(app);
server.listen(1337, function() { });

wsServer = new WebSocketServer({
    httpServer: server
});

wsServer.on('request', function(request) {
    console.log((new Date()) + ' Connection from origin '
        + request.origin + '.');

    var connection = request.accept(null, request.origin);

    console.log((new Date()) + ' Connection accepted.');

    // connection.sendUTF(JSON.stringify({ text: "hi" }));

    // This is the most important callback for us, we'll handle
    // all messages from users here.
    connection.on('message', function(message) {
        console.log(message);
        connection.sendUTF(JSON.stringify({ text: "hi" }));
    });

    connection.on('close', function(connection) {
        // close user connection
        console.log("closing");
    });
});

module.exports = app;
