const express = require('express');
const router = express.Router();
const path = require('path');
const webpush = require('web-push');

// push notifications API
const publicVapidKey =
    "BDA2lsxEfak7Pe8mNKfsc7uRVJb8T3cllZbYAj8dKdF8SckOGPSijyFYJTHShD0ZHmBYbn6laOwLxpZ9gGfliHw";
const privateVapidKey = "EI1gwugjoQ1YVWNaG9yM2ZYCkMa0UhtBB7tvlLgz2qI";

webpush.setVapidDetails(
    "mailto:test@test.com",
    publicVapidKey,
    privateVapidKey
);

var subscriptions = [];
/* GET home page. */
// router.get('', function(req, res, next) {
//   res.render('index', { title: 'Express' });
// });

router.get('',function(req,res){
    res.sendFile(path.join(__dirname+'../../../front/html/home.html'));
});

router.post("/subscribe", (req, res) => {
    // Get pushSubscription object
    const subscription = req.body;

    subscriptions.push(subscription);

    console.log(subscription);

    // Send 201 - resource created
    res.status(201).json({});

    // Create payload
    const payload = JSON.stringify({ title: "Push Test" });

    subscriptions.forEach(function(sub, index, allSubscriptions) {
        webpush
            .sendNotification(sub, payload)
            .catch(err => console.error(err));
    });
    // Pass object into sendNotification
    // webpush
    //     .sendNotification(subscription, payload)
    //     .catch(err => console.error(err));
});

module.exports = router;
