const express = require('express');
const router = express.Router();
const database = require('../model/data-model');

router.put('/understood/:token/:id', (req, res) => {
    const shouldUpdate = function(){
        database.db.updateStudentFeedbackUnderstood(params.user_id, params.token, successfulUpdate, unsuccessfulUpdate);
    };

    const shouldInsert = function() {
        database.db.insertFirstFeedBackPositive(params.user_id, params.token, successfulUpdate, unsuccessfulUpdate);
    };

    const successfulUpdate = function(){
        res.statusCode = 200;
        res.end("ok");
    };

    const unsuccessfulUpdate = function() {
        res.statusCode = 400;
        res.end("You can vote every 5 minutes");
    };

    const params = {
        user_id: req.params.id,
        token: req.params.token
    };

    database.db.userHasVotedOnce(params.user_id, params.token, shouldUpdate, shouldInsert);
    console.log("PUT /feedback/:token/:id");
});

router.put('/notunderstood/:token/:id', (req, res) => {
    const shouldUpdate = function(){
        database.db.updateStudentFeedbackNotunderstood(params.user_id, params.token, successfulUpdate, unsuccessfulUpdate);
    };

    const shouldInsert = function() {
        database.db.insertFirstFeedBackNegative(params.user_id, params.token, successfulUpdate, unsuccessfulUpdate);
    };

    const successfulUpdate = function(){
        res.statusCode = 200;
        res.end("ok");
    };

    const unsuccessfulUpdate = function() {
        res.statusCode = 400;
        res.end("You can vote every five minutes");
    };

    const params = {
        user_id: req.params.id,
        token: req.params.token
    };

    database.db.userHasVotedOnce(params.user_id, params.token, shouldUpdate, shouldInsert);
    console.log("PUT /feedback/:token/:id");
});

router.get('/:token', (req, res) => {
    const validGet = function(data){
        res.statusCode = 200;
        res.end(JSON.stringify(data.rows[0]));
    };

    const invalidGet = function() {
        res.statusCode = 404;
        res.end("not found");
    };

    database.db.needsExplanation( req.params.token, validGet, invalidGet);
    console.log("GET /feedback/:token");
});

module.exports = router;
