const express = require('express');
const router = express.Router();
const database = require('../model/data-model');

router.post('/student', (req, res) => {
    const validAdd = function(){
        res.statusCode = 201;
        res.end('question added to db');
    };

    const invalidAdd = function() {
        res.statusCode = 500;
        res.end("failure");
    };

    const params = {
        question: req.body.question,
        student_id: req.body.student_id,
        token: req.body.token,
        anonymous: req.body.anonymous
    };

    database.db.addStudentQuestion(params.question, params.student_id, params.token, params.anonymous, validAdd, invalidAdd);
    console.log("POST /question/student");
});

router.post('/teacher', (req, res) => {
    const validAdd = function(){
        res.statusCode = 201;
        res.end('question added to db');
    };

    const invalidAdd = function() {
        res.statusCode = 500;
        res.end("failure");
    };

    const params = {
        question: req.body.question,
        teacher_id: req.body.teacher_id,
        token: req.body.token,
        opt1: req.body.opt1,
        opt2: req.body.opt2,
        opt3: req.body.opt3,
        opt4: req.body.opt4,
    };

    database.db.addTeacherQuestion(params.question, params.teacher_id, params.token,params.opt1, params.opt2, params.opt3, params.opt4, validAdd, invalidAdd);
    console.log("POST /question/teacher");
});

router.post('/student/answer', (req, res) => {
    const validAdd = function(){
        res.statusCode = 201;
        res.end('answer added to db');
    };

    const invalidAdd = function() {
        res.statusCode = 500;
        res.end("failure");
    };

    const params = {
        student_id: req.body.student_id,
        question_id: req.body.question_id,
        answer_value: req.body.answer_value
    };

    database.db.addStudentAnswer(params.student_id, params.question_id, params.answer_value, validAdd, invalidAdd);
    console.log("POST /question/student/answer");
});

router.get('/student/answer/:id', (req, res) => {
    const validAnswer = function(){
        res.statusCode = 204;
        res.end(null);
    };

    const invalidAnswer = function() {
        res.statusCode = 400;
        res.end("already answered");
    };

    const params = {
        user_id: req.headers['user_id'],
        question_id: req.params.id
    };

    database.db.hasAlreadyAnswered(params.user_id, params.question_id, validAnswer, invalidAnswer);
    console.log("GET /questions/student/answer/:id");
});

router.get('/teacher/answer', (req, res) => {
    const validGet = function(result){
        res.statusCode = 200;
        res.end(JSON.stringify(result.rows));
    };

    const invalidGet = function() {
        res.statusCode = 204;
        res.end("no results");
    };

    const params = {
        token: req.headers['token'],
        id: req.headers['user_id']
    };

    database.db.getStudentAnswers(params.token, params.id, validGet, invalidGet);
    console.log("GET /questions/teacher/answer");
});

router.get('/teacher', (req,res) => {
    const validGet = function(result){
        res.statusCode = 200;
        res.end(JSON.stringify(result.rows));
    };

    const invalidGet = function() {
        res.statusCode = 204;
        res.end("no results");
    };

    const params = {
        token: req.headers['token'],
        likes: req.headers['likes']
    };

    database.db.getNeedResponseQuestions(params.token, params.likes, validGet, invalidGet);
    console.log("GET /questions/teacher");
});

router.get('/teacher/:id', (req, res) => {
    const validGet = function(data){
        res.statusCode = 200;
        res.end(JSON.stringify(data.rows[0]));
    };

    const invalidGet = function() {
        res.statusCode = 404;
        res.end("not found");
    };

    database.db.lastAskedQuestion( req.params.id, validGet, invalidGet);
    console.log("GET /questions/teacher/:id");
});

router.get('/student', (req,res) => {
    const validGet = function(result){
        res.statusCode = 200;
        res.end(JSON.stringify(result.rows));
    };

    const invalidGet = function() {
        res.statusCode = 204;
        res.end("no results");
    };

    const params = {
        token: req.headers['token'],
        id: req.headers['student_id']
    };

    database.db.getSessionStudentQuestions(params.token, params.id, validGet, invalidGet);
    console.log("GET /questions/student");
});

router.get('/student/answer', (req, res) => {
    const validGet = function(result){
        res.statusCode = 200;
        res.end(JSON.stringify(result.rows[0]));
    };

    const invalidGet = function() {
        res.statusCode = 204;
        res.end("no results");
    };

    const params = {
        token: req.headers['token']
    };

    database.db.teacherRequestsAnswer(params.token, validGet, invalidGet);
    console.log("GET /questions/student/answer");
});

router.get('/student/:id', (req,res) => {
    const validGet = function(result){
        res.statusCode = 200;
        res.end(JSON.stringify(result.rows));
    };

    const invalidGet = function() {
        res.statusCode = 204;
        res.end("no results");
    };

    const params = {
        token: req.headers['token'],
        id: req.params.id
    };

    database.db.getStudentQuestions(params.token, params.id, validGet, invalidGet);
    console.log("GET /questions/student/:id");
});

router.get('/student/:id', (req,res) => {
    const validGet = function(result){
        res.statusCode = 200;
        res.end(JSON.stringify(result.rows));
    };

    const invalidGet = function() {
        res.statusCode = 204;
        res.end("no results");
    };

    database.db.getSessionStudentQuestions(req.params.id, validGet, invalidGet);
    console.log("GET /questions/student/:id");
});

router.patch('/student/like/:id', (req, res) => {
    const successfulLike = function(){
        res.statusCode = 200;
        res.end("updated");
    };

    const unsuccessfulLike = function() {
        res.statusCode = 404;
        res.end("there was a problem when updating");
    };

    database.db.likeQuestion(req.params.id, successfulLike, unsuccessfulLike);
});

router.patch('/student/dislike/:id', (req, res) => {
    const successfulDislike = function(){
        res.statusCode = 200;
        res.end("updated");
    };

    const unsuccessfulDislike = function() {
        res.statusCode = 404;
        res.end("there was a problem when updating");
    };

    database.db.dislikeQuestion(req.params.id, successfulDislike, unsuccessfulDislike);
});

module.exports = router;
