const express = require('express');
const router = express.Router();
const database = require('../model/data-model');

router.post('/', (req, res) => {
    const validRegister = function(){
        database.db.login(user.email, user.parola, validLogin, invalidRegister);
    };

    const invalidRegister = function() {
        res.statusCode = 500;
        res.end("failure");
    };

    const validLogin = function(data){
        let user_id = data[0];
        let user_role = data[1];
        res.statusCode = 201;
        res.cookie('user_id', user_id);
        res.cookie('user_role', user_role);
        res.end('user exists');
    };

    const user = {
        person_role: req.body.person_role,
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        email: req.body.email,
        university: req.body.university,
        faculty: req.body.faculty,
        parola: req.body.password
    };

    database.db.registerUser(user.person_role, user.first_name, user.last_name, user.email, user.university, user.faculty, user.parola, validRegister, invalidRegister);
    console.log("POST /users");
});

router.post('/login', (req, res) => {
    const validLogin = function(data){
        let user_id = data[0];
        let user_role = data[1];
        res.statusCode = 200;
        res.cookie('user_id', user_id);
        res.cookie('user_role', user_role);
        res.end('user exists');
    };

    const invalidLogin = function() {
        res.statusCode = 404;
        res.end("failure");
    };

    const user = {
        email: req.body.email,
        parola: req.body.password
    };

    database.db.login(user.email, user.parola, validLogin, invalidLogin);
    console.log("POST /users/login");
});

router.get('/all/:token', (req, res) => {
    const validGet = function(result){
        res.statusCode = 200;
        res.end(JSON.stringify(result.rows[0]));
    };

    const invalidGet = function() {
        res.statusCode = 404;
        res.end("no students logged into this course");
    };

    const params = {
        token: req.params.token
    };

    database.db.loggedStudents(params.token,  validGet, invalidGet);
    console.log("GET /questions/student/answer/:id");
});

router.get('/:id', (req, res) => {
    const validGet = function(result){
        res.statusCode = 200;
        res.end(JSON.stringify(result.rows[0]));
    };

    const invalidGet = function() {
        res.statusCode = 404;
        res.end("student not found");
    };

    database.db.getIdentity(req.params.id,  validGet, invalidGet);
    console.log("GET /questions/student/:id");
});

router.patch('/:id', (req,res) => {
    const successfulUpdate = function(){
        res.statusCode = 200;
        res.end("updated");
    };

    const unsuccessfulUpdate = function() {
        res.statusCode = 400;
        res.end("there was a problem when updating");
    };

    const params = {
        token: req.headers['token'],
        user_id: req.params.id
    };

    database.db.updateTokenForStudent(params.user_id, params.token, successfulUpdate, unsuccessfulUpdate);
});

router.patch('/teacher/:id', (req, res) => {
    const successfulUpdate = function(){
        res.statusCode = 200;
        res.end("updated");
    };

    const unsuccessfulUpdate = function() {
        res.statusCode = 400;
        res.end("there was a problem when updating");
    };

    const params = {
        percentage: req.headers['percentage'],
        user_id: req.params.id
    };

    database.db.updateTeacherPercentage(params.user_id, params.percentage, successfulUpdate, unsuccessfulUpdate);
});

module.exports = router;
