const express = require('express');
const router = express.Router();
const database = require('../model/data-model');

router.post('/create', (req, res) => {
    const successfulCreation = function(){
        res.statusCode = 201;
        res.end('token created');
    };

    const unsuccessfulCreation = function() {
        res.statusCode = 500;
        res.end("failure");
    };

    const params = {
        teacher_id: req.body.teacher_id,
        token: req.body.token
    };

    database.db.createToken(params.teacher_id, params.token, successfulCreation, unsuccessfulCreation);
    console.log("POST /token/create");
});

router.get('/', (req, res) => {
    const successfulRetrieve = function(){
        res.statusCode = 200;
        res.end('token retrieved');
    };

    const unsuccessfulRetrieve = function() {
        res.statusCode = 404;
        res.end("token not found");
    };

    let token= req.headers['token'];
    database.db.retrieveToken(token, successfulRetrieve, unsuccessfulRetrieve);
    console.log("GET /token");
});

module.exports = router;
