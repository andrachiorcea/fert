const oracledb = require('oracledb');

const db = function() {
    const closeConnection = function(connection) {
        connection.close(
            function(err) {
                if (err)
                    console.error(err.message);
            });
    };

    function executeQuery(query, params, processResult) {
        oracledb.getConnection(
            {
                user          : "student",
                password      : "parola",
                connectString : "localhost:1521/XE"
            },
            function(err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    query,
                    params,
                    function(err, result) {
                        if (err) {
                            console.error(err.message);
                            closeConnection(connection);
                            return;
                        }
                        processResult(result);
                        closeConnection(connection);
                    });
            });
    }

    function executeInsert(query, params, successfulInsert, unsuccessfulInsert) {
        oracledb.getConnection(
            {
                user          : "student",
                password      : "parola",
                connectString : "localhost:1521/XE"
            },
            function(err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    query,
                    params,
                    { autoCommit: true},
                    function(err, result) {
                        if (err) {
                            console.error(err.message);
                            closeConnection(connection);
                            return;
                        }
                        if (result.rowsAffected !== 1 ) {
                            console.log('Not inserted');
                            unsuccessfulInsert();
                            closeConnection(connection);
                            return;
                        }
                        else {
                            console.log('1 row inserted');
                            successfulInsert(params);
                            closeConnection(connection);
                            return;
                        }
                    });
            });
    }

    function executeDelete (query, params, successfulDelete, unsuccessfulDelete) {
        oracledb.getConnection(
            {
                user          : "student",
                password      : "parola",
                connectString : "localhost:1521/XE"
            },
            function(err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    query,
                    params,
                    { autoCommit: true},
                    function(err, result) {
                        if (err) {
                            console.error(err.message);
                            closeConnection(connection);
                            return;
                        }
                        if (result.rowsAffected === 1 ) {
                            console.log('1 row deleted');
                            successfulDelete();
                            closeConnection(connection);
                            return;
                        }
                        else {
                            console.log('not deleted');
                            unsuccessfulDelete();
                            closeConnection(connection);
                            return;
                        }
                    });
            });
    }

    function executeUpdate (query, params, successfulUpdate, unsuccessfulUpdate) {
        oracledb.getConnection(
            {
                user          : "student",
                password      : "parola",
                connectString : "localhost:1521/XE"
            },
            function(err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    query,
                    params,
                    { autoCommit: true},
                    function(err, result) {
                        if (err) {
                            console.error(err.message);
                            closeConnection(connection);
                            return;
                        }
                        if (result.rowsAffected != 1 ) {
                            console.log('not updated');
                            unsuccessfulUpdate();
                            closeConnection(connection);
                            return;
                        }
                        else {
                            console.log('updated');
                            successfulUpdate();
                            closeConnection(connection);
                            return;
                        }
                    });
            });
    }

    function login(email, password, validLogin, invalidLogin) {
        const params = {email: email, password: password};
        const processResult = function(data) {
            if(data.rows.length === 0) {
                invalidLogin();
                console.log('nu exista user')
            }
            else {
                validLogin(data.rows[0]);
                console.log('avem useri');
            }
        };
        executeQuery("SELECT id, person_role FROM USERS WHERE email = :email and parola = :password", params, processResult);
    }

    function registerUser(person_role, first_name,last_name, email, university, faculty, parola, validRegister, invalidRegister) {
        const params = {
            person_role: person_role,
            first_name: first_name,
            last_name: last_name,
            email: email,
            university: university,
            faculty: faculty,
            parola: parola
        };
    executeInsert("INSERT INTO USERS (id, person_role, first_name, last_name, email, university, faculty, parola) VALUES(NVL((SELECT max(id) + 1 FROM USERS), 1),:person_role, :first_name, :last_name, :email, :university, :faculty, :parola)", params, validRegister, invalidRegister);
    }

    function createToken(teacher_id, token , successfulCreation, unsuccessfulCreation) {
        const params = {id: teacher_id, token: token};
        executeUpdate("UPDATE USERS set current_session_token =: token where id = :id", params, successfulCreation, unsuccessfulCreation);
    }

    function retrieveToken(token, successfulRetrieve, unsuccessfulRetrieve) {
        const params = {token: token};
        let processResult = function(data) {
            if (data.rows.length === 0) {
                unsuccessfulRetrieve();
            }
            else {
                successfulRetrieve();
            }
        };
        executeQuery("SELECT current_session_token FROM USERS WHERE current_session_token =: token", params, processResult);
    }

    function addStudentQuestion(question, student_id, token, anonymous, validAdd, invalidAdd) {
        const params = {
            question: question,
            student_id: student_id,
            token: token,
            anonymous: anonymous
        };
        executeInsert("INSERT INTO student_questions (squestion_id, squestion, student_id, session_token, like_count, dislike_count, anonymous) values (nvl((SELECT max(squestion_id) + 1 FROM student_questions), 1), :question, :student_id, :token, 0, 0, :anonymous)", params, validAdd, invalidAdd);
    }

    function addTeacherQuestion(question, teacher_id, token, opt1, opt2, opt3, opt4, validAdd, invalidAdd) {
        const params = {
            question: question,
            teacher_id: teacher_id,
            token: token,
            opt1: opt1,
            opt2: opt2,
            opt3: opt3,
            opt4: opt4
        };
        executeInsert("INSERT INTO teacher_questions (tquestion_id, tquestion, teacher_id, session_token, option1, option2, option3, option4, timestamps) values (nvl((SELECT max(tquestion_id) + 1 FROM teacher_questions), 1), :question, :teacher_id, :token, :opt1, :opt2, :opt3, :opt4, CURRENT_TIMESTAMP)", params, validAdd, invalidAdd);
    }

    function addStudentAnswer(student_id, question_id, answer_value, validAdd, invalidAdd) {
        const params = {
            student_id: student_id,
            question_id: question_id,
            answer_value: answer_value
        };
        executeInsert("INSERT INTO student_answers (student_id, tquestion_id, answer_value) values(:student_id, :question_id, :answer_value)", params, validAdd, invalidAdd);
    }

    function getSessionStudentQuestions(token, id, validGet, invalidGet) {
        const params = {token: token, id: id};
        console.log(params);
        let processResult = function (data) {
            if (data.rows.length === 0) {
                invalidGet();
            }
            else {
                validGet(data);
            }
        };
        executeQuery("SELECT squestion_id, squestion, anonymous, student_id  FROM student_questions WHERE session_token =: token and student_id != :id", params, processResult);
    }

    function getStudentQuestions(token, id, validGet, invalidGet) {
        const params = {token: token, id: id};
        console.log(params);
        let processResult = function (data) {
            if (data.rows.length === 0) {
                invalidGet();
            }
            else {
                validGet(data);
            }
        };
        executeQuery("SELECT squestion_id, squestion FROM student_questions WHERE session_token =: token and student_id = :id", params, processResult);
    }

    function getNeedResponseQuestions(token, likes, validGet, invalidGet) {
        const params = {
            token: token,
            likes: likes};
        let processResult = function (data) {
            if (data.rows.length === 0) {
                invalidGet();
            }
            else {
                validGet(data);
            }
        };
        executeQuery("SELECT squestion_id, squestion, ANONYMOUS, student_id FROM student_questions WHERE session_token =: token and like_count >= :likes", params, processResult);
    }

    function lastAskedQuestion(id, validGet, invalidGet) {
        const params = {
            id: id,
        };

        let processResult = function (data) {
            if (data.rows.length === 0) {
                invalidGet();
            }
            else {
                validGet(data);
            }
        };

        executeQuery("select * from (SELECT tquestion, option1, option2, option3, option4 from teacher_questions where" +
            " teacher_id =: id order by tquestion_id desc) where rownum <=1", params, processResult);
    }

    function teacherRequestsAnswer(token, validGet, invalidGet) {
        const params = {token: token};
        let processResult = function (data) {
            if (data.rows.length === 0) {
                invalidGet();
            }
            else {
                validGet(data);
            }
        };
        executeQuery("SELECT tquestion_id, tquestion, option1, option2, option3, option4 FROM teacher_questions WHERE session_token =: token order by tquestion_id desc", params, processResult);
    }

    function hasAlreadyAnswered(user_id, question_id, validAnswer, invalidAnswer) {
        const params = {
            question_id: question_id,
            user_id: user_id
        };
        let processResult = function (data) {
            if (data.rows.length === 0) {
                validAnswer();
            }
            else {
                invalidAnswer();
            }
        };
        executeQuery("SELECT * FROM student_answers WHERE tquestion_id =: question_id and student_id =: user_id", params, processResult);
    }

    function loggedStudents(token,  validGet, invalidGet) {
        const params = {token: token};
        let processResult = function (data) {
            if (data.rows.length === 0) {
                invalidGet();
            }
            else {
                validGet(data);
            }
        };
        executeQuery("SELECT count(*) from users WHERE person_role ='Student' and current_session_token =: token", params, processResult);
    }

    function getIdentity(id,  validGet, invalidGet) {
        const params = {id: id};
        let processResult = function (data) {
            if (data.rows.length === 0) {
                invalidGet();
            }
            else {
                validGet(data);
            }
        };
        executeQuery("SELECT first_name, last_name from users where id =: id", params, processResult);
    }

    function getStudentAnswers(token,id,  validGet, invalidGet) {
        const params = {
            token: token,
            id: id};
        let processResult = function (data) {
            if (data.rows.length === 0) {
                invalidGet();
            }
            else {
                validGet(data);
            }
        };
        executeQuery("select count(student_id), answer_value from student_answers where tquestion_id = " +
            "(select * from (SELECT tquestion_id from teacher_questions where" +
            " teacher_id =: id and session_token =: token order by tquestion_id desc) where rownum<=1) group by answer_value"
              , params, processResult);
}

    function likeQuestion(question_id, successfulLike, unsuccessfulLike) {
        const params = {
            question_id: question_id
        };
        executeUpdate("Update student_questions set like_count = like_count + 1 where squestion_id =: question_id", params, successfulLike, unsuccessfulLike);
    }

    function dislikeQuestion(question_id, successfulDislike, unsuccessfulDislike) {
        const params = {
            question_id: question_id
        };
        executeUpdate("Update student_questions set dislike_count = dislike_count + 1 where squestion_id =: question_id", params, successfulDislike, unsuccessfulDislike);
    }

    function updateTokenForStudent(user_id, token, successfulUpdate, unsuccessfulUpdate) {
        const params = {
            user_id: user_id,
            token: token
        };
        executeUpdate("UPDATE USERS SET current_session_token =: token where id =: user_id", params, successfulUpdate, unsuccessfulUpdate);
    }

    function userHasVotedOnce(user_id, token, shouldUpdate, shouldInsert) {
        const params = {
            user_id: user_id,
            token: token
        };

        let processResult = function (data) {
            if (data.rows.length === 0) {
                shouldInsert();
            }
            else {
                shouldUpdate(data);
            }
        };

        executeQuery("SELECT student_id, token from student_feedback where student_id =: user_id and token =: token", params, processResult);
    }

    function updateStudentFeedbackUnderstood(user_id, token, successfulUpdate, unsuccessfulUpdate) {
        const params = {
            user_id: user_id,
            token: token
        };
        executeUpdate("update student_feedback set ts_def = CURRENT_TIMESTAMP," +
            " AM_INTELES_COUNT = AM_INTELES_COUNT + 1" +
            "where student_id =: user_id and token =:token" +
            " and " +
            "(select round((cast(current_timestamp as date) -" +
            " cast((select ts_def from student_feedback where student_id = 31)as date)) * 24 * 60) " +
            "from dual) >=5", params, successfulUpdate, unsuccessfulUpdate);
    }

    function insertFirstFeedBackPositive(user_id, token, successfulUpdate,unsuccessfulUpdate) {
        const params = {
            user_id: user_id,
            token: token
        };
        executeInsert("insert into student_feedback (student_id, token, am_inteles_count" +
            ", nam_inteles_count, ts_def) values (:user_id, :token, 1, 0, CURRENT_TIMESTAMP)", params, successfulUpdate, unsuccessfulUpdate);
    }

    function updateStudentFeedbackNotunderstood(user_id, token, successfulUpdate, unsuccessfulUpdate) {
        const params = {
            user_id: user_id,
            token: token
        };
        executeUpdate("update student_feedback set ts_def = CURRENT_TIMESTAMP," +
            " NAM_INTELES_COUNT = NAM_INTELES_COUNT + 1" +
            "where student_id =: user_id and token =:token and (select" +
            "  round(" +
            "    (cast(current_timestamp as date) - cast((select ts_def from student_feedback where student_id = 31)as date))\n" +
            "    * 24 * 60" +
            "  )" +
            "from dual) >=5", params, successfulUpdate, unsuccessfulUpdate);
    }

    function insertFirstFeedBackNegative(user_id, token, successfulUpdate,unsuccessfulUpdate) {
        const params = {
            user_id: user_id,
            token: token
        };
        executeInsert("insert into student_feedback (student_id, token, am_inteles_count" +
            ", nam_inteles_count, ts_def) values (:user_id, :token, 0, 1, CURRENT_TIMESTAMP)", params, successfulUpdate, unsuccessfulUpdate);
    }

    function updateTeacherPercentage(id, percentage, successfulUpdate,unsuccessfulUpdate) {
        const params = {
            user_id: id,
            percentage: percentage
        };
        executeInsert("update users set percentage =: percentage where id =: user_id", params, successfulUpdate, unsuccessfulUpdate);
    }

    function needsExplanation(token, sucessfulQuery, unsucessfulQuery) {
        const params = {
            token: token,
        };

        let processResult = function (data) {
            if (data.rows.length === 0) {
                unsucessfulQuery();
            }
            else {
                sucessfulQuery(data);
            }
        };

        executeQuery("SELECT sum(nam_inteles_count)-sum(am_inteles_count) from student_feedback where token =: token", params, processResult);
    }

    return {
        login: login,
        registerUser: registerUser,
        createToken: createToken,
        retrieveToken: retrieveToken,
        addStudentQuestion: addStudentQuestion,
        getSessionStudentQuestions: getSessionStudentQuestions,
        getStudentQuestions: getStudentQuestions,
        likeQuestion: likeQuestion,
        dislikeQuestion: dislikeQuestion,
        getNeedResponseQuestions: getNeedResponseQuestions,
        addTeacherQuestion: addTeacherQuestion,
        teacherRequestsAnswer: teacherRequestsAnswer,
        getStudentAnswers: getStudentAnswers,
        addStudentAnswer: addStudentAnswer,
        hasAlreadyAnswered: hasAlreadyAnswered,
        loggedStudents: loggedStudents,
        updateTokenForStudent: updateTokenForStudent,
        lastAskedQuestion: lastAskedQuestion,
        getIdentity: getIdentity,
        updateTeacherPercentage: updateTeacherPercentage,
        userHasVotedOnce: userHasVotedOnce,
        updateStudentFeedbackUnderstood: updateStudentFeedbackUnderstood,
        insertFirstFeedBackPositive: insertFirstFeedBackPositive,
        updateStudentFeedbackNotunderstood: updateStudentFeedbackNotunderstood,
        insertFirstFeedBackNegative: insertFirstFeedBackNegative,
        needsExplanation: needsExplanation
    }
}();

module.exports = {
    db: db
};