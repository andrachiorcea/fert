document.addEventListener("DOMContentLoaded", function(){
    loadTeacherQuestion();
});

setTimeout(function(){
    window.location.reload();
}, 300000);

var inputs = [];

function loadTeacherQuestion() {
    const xhr = new XMLHttpRequest();
    xhr.open("GET", '/API/question/student/answer', true);
    xhr.setRequestHeader("Content-Type", "application/json");
    const token = getCookie('token');
    xhr.setRequestHeader("token",token);

    xhr.onreadystatechange = function() { // Call a function when the state changes.
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            createQuestionCards(JSON.parse(this.response));
        }
    };

    xhr.send(null);
}

function createQuestionCards(data) {
    let to_append = document.getElementById('form2');

    let question_id = data[0];
    let question_content = data[1];
    let options = [data[2], data[3], data[4], data[5]];
    let valid_options = [];

    for (let i=0; i<options.length; i++) {
        if (options[i]  !== null) valid_options.push(options[i]);
    }

    let question = document.createElement('p');
    question.id = question_id;
    question.innerText = question_content;
    to_append.appendChild(question);

    for(let j=0; j<valid_options.length; j++) {
        let label = document.createElement('label');
        label.className = 'container';
        label.id = 'check' + j;

        let input = document.createElement('input');
        input.type = 'checkbox';
        input.name = 'answer' + j;
        inputs.push(input);
        label.addEventListener('click', function(){
            disableRestCheckboxes(input);
        });

        let div = document.createElement('div');
        div.innerText = valid_options[j];

        let span = document.createElement('span');
        span.className = 'checkmark';

        label.appendChild(input);
        label.appendChild(div);
        label.appendChild(span);
        to_append.appendChild(label);
    }

    let div = document.createElement('div');
    div.className = 'buttons';
    let button = document.createElement('button');
    button.type = 'button';
    button.innerText = 'Submit';
    button.id = 'submit-response';
    button.onclick = checkAlreadyVoted;
    div.appendChild(button);
    to_append.appendChild(div);
}

function disableRestCheckboxes(input) {
    for(let j=0; j< inputs.length; j++) {
        if (input!==inputs[j]) {
            inputs[j].disabled = true;
            inputs[j].checked = false;
        }
        else {
            inputs[j].checked = true;
            inputs[j].disabled = false;
        }
    }
}

function checkAlreadyVoted() {
    const xhr = new XMLHttpRequest();
    const student_id = getCookie('user_id');
    if (student_id === undefined) {
        alert("Log in first");
        window.location.href = "http://localhost:3000/html/home.html";
        return;
    }
    const question = document.getElementsByTagName('p')[0].getAttribute('id');
    const route = '/API/question/student/answer/' + question;

    xhr.open("GET", route , true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("user_id",student_id);

    xhr.onreadystatechange = function() { // Call a function when the state changes.
        if (this.readyState === XMLHttpRequest.DONE && this.status === 204) {
            addAnswer();
        }
        else if(this.readyState === XMLHttpRequest.DONE && this.status !== 204){
            alert("You've already answered this question");
        }
    };

    xhr.send(null);
}

function addAnswer()
{
    const question = document.getElementsByTagName('p')[0];
    const data = {};
    let id = getCookie('user_id');
    if (id === undefined) {
        alert("Log in first");
        window.location.href = "http://localhost:3000/html/home.html";
        return;
    }
    data['student_id'] = id;
    data['question_id'] = question.getAttribute('id');

    for (let i=0; i<inputs.length; i++) {
        if(inputs[i].checked) {
            let div = inputs[i].nextSibling;
            data['answer_value'] = div.innerText;
            break;
        }
    }

    if(!data['answer_value']) {
        alert("You must check an option!")
    }
    else {
        const xhr = new XMLHttpRequest();
        xhr.open("POST", '/API/question/student/answer', true);
        xhr.setRequestHeader("Content-Type", "application/json");

        xhr.onreadystatechange = function() { // Call a function when the state changes.
            if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
                // createQuestionCards(JSON.parse(this.response));
            }
        };

        xhr.send(JSON.stringify(data));
    }
}