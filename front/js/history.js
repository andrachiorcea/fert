document.addEventListener("DOMContentLoaded", function(){
    loadQuestions();
});

function loadQuestions() {
    const xhr = new XMLHttpRequest();
    const student_id = getCookie('user_id');
    if (student_id === undefined) {
        alert("Log in first");
        window.location.href = "http://localhost:3000/html/home.html";
        return;
    }
    const route = '/API/question/student/' + student_id;

    xhr.open("GET", route , true);
    xhr.setRequestHeader("Content-Type", "application/json");
    const token = getCookie('token');
    xhr.setRequestHeader("token",token);

    xhr.onreadystatechange = function() { // Call a function when the state changes.
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            createQuestionCards(JSON.parse(this.response));
        }
    };

    xhr.send(null);
}

function createQuestionCards(data) {
    let to_append = document.getElementsByClassName('card-container')[0];

    for(let i=0; i<data.length; i++) {
        let question_id = data[i][0];
        let question_content = data[i][1];

        let card = document.createElement('div');
        let question = document.createElement('p');

        card.style.transform = "rotate(" + (Math.random() < 0.5 ? -1 : 1) * Math.random() * 10 + "deg)";
        card.className = 'card';
        card.id = 'card-' + question_id;
        question.className = 'question';
        question.id = 'question-' + question_id;
        question.innerText = question_content;

        card.appendChild(question);
        to_append.appendChild(card);
    }
}
