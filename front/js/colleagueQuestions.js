// Get the modal and its corresponding button
var modal = document.getElementById('modal');
var btn = document.getElementById("button-modal");
var add_question_button = document.getElementById("add-question");
var span = document.getElementsByClassName("close")[0];


// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
};

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
};

// Get the modal and its corresponding button
var modal2 = document.getElementById('modal-second');
var btn = document.getElementById("button-modal");
var add_question_button = document.getElementById("add-question");
var span = document.getElementsByClassName("close")[0];

var feedback = document.getElementById("button-modal2");
var btn_feed_positive = document.getElementById("understand");
var btn_feed_negative = document.getElementById("dont-understand");

// When the user clicks the button, open the modal 
btn.onclick = function(event) {
    event.preventDefault();
    modal.style.display = "block";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
    if (event.target == modal2) {
        modal2.style.display = "none";
    }
}
// Get the modal and its corresponding button

// When the user clicks the button, open the modal 
feedback.onclick= function(event) {
    event.preventDefault();
    modal2.style.display = "block";
};

setTimeout(function(){
    window.location.reload();
}, 300000);

document.addEventListener("DOMContentLoaded", function(){
    loadQuestions();
});

function loadQuestions() {
    const xhr = new XMLHttpRequest();
    xhr.open("GET", '/API/question/student', true);
    xhr.setRequestHeader("Content-Type", "application/json");
    const token = getCookie('token');
    const student_id = getCookie('user_id');
    if (student_id === undefined) {
        alert("Log in first");
        window.location.href = "http://localhost:3000/html/home.html";
        return;
    }
    xhr.setRequestHeader("token",token);
    xhr.setRequestHeader("student_id",student_id);

    xhr.onreadystatechange = function() { // Call a function when the state changes.
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            createQuestionCards(JSON.parse(this.response));
        }
    };

    xhr.send(null);
}

function addQuestion(obj) {
    const question = document.getElementById('squestion').value;
    const id = getCookie('user_id');
    if (id === undefined) {
        alert("Log in first");
        window.location.href = "http://localhost:3000/html/home.html";
        return;
    }
    const session = getCookie('token');
    const anonymous = document.getElementById('anonymous').checked;
    const data = {};
    data['question'] = question;
    data['student_id'] = id;
    data['token'] = session;
    data['anonymous'] = anonymous ?  1 : 0;

    const xhr = new XMLHttpRequest();
    xhr.open("POST", '/API/question/student', true);
    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.onreadystatechange = function() { // Call a function when the state changes.
        if (this.readyState === XMLHttpRequest.DONE && this.status === 201) {
            modal.style.display = "none";
        }
        else if (this.readyState === XMLHttpRequest.DONE && this.status !== 201) {
            window.alert("Server side error");
        }
    };

    xhr.send(JSON.stringify(data));
}


function likeQuestion(id) {
    const question_id = id.split("-")[1];
    const route = '/API/question/student/like/' + question_id;

    const xhr = new XMLHttpRequest();
    xhr.open("PATCH", route, true);
    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.onreadystatechange = function() { // Call a function when the state changes.
        // if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
        //     modal.style.display = "none";
        // }
        // else if (this.readyState === XMLHttpRequest.DONE && this.status !== 201) {
        //     window.alert("Server side error");
        // }
    };

    xhr.send(null);
}

function dislikeQuestion(id) {
    const question_id = id.split("-")[1];
    const route = '/API/question/student/dislike/' + question_id;

    const xhr = new XMLHttpRequest();
    xhr.open("PATCH", route, true);
    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.onreadystatechange = function() { // Call a function when the state changes.
        // if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
        //     modal.style.display = "none";
        // }
        // else if (this.readyState === XMLHttpRequest.DONE && this.status !== 201) {
        //     window.alert("Server side error");
        // }
    };

    xhr.send(null);
}

function createQuestionCards(data) {
    let to_append = document.getElementsByClassName('card-container-vote')[0];

    for(let i=0; i<data.length; i++) {
        let question_id = data[i][0];
        let question_content = data[i][1];
        let anonimity = data[i][2];
        let student_id = data[i][3];
        let question_already_voted = sessionStorage.getItem(question_id);

        let card = document.createElement('div');
        let question = document.createElement('p');
        let buttons = document.createElement('div');
        let like = document.createElement('div');
        let dislike = document.createElement('div');

        card.style.transform = "rotate(" + (Math.random() < 0.5 ? -1 : 1) * Math.random() * 10 + "deg)";
        card.className = 'card';
        card.id = 'card-' + question_id;
        question.className = 'question';
        question.id = 'question-' + question_id;
        question.innerText = question_content;
        buttons.className = 'buttons';
        like.className = 'like';
        like.id = 'like-' + question_id;
        dislike.className = 'dislike';
        dislike.id = 'dislike-' + question_id;

        if (question_already_voted === 'liked') {
            like.style.background = "url(../images/thumbs_up_clicked.png) no-repeat";
            like.onclick = false;
            dislike.onclick = false;
        }
        else if (question_already_voted === 'disliked') {
            dislike.style.background = "url(../images/thumbs_down_clicked.png) no-repeat";
            like.onclick = false;
            dislike.onclick = false;
        }

        like.onclick = function() {
            like.style.background = "url(../images/thumbs_up_clicked.png) no-repeat";
            dislike.style.background = "url(../images/thumbs_down.png) no-repeat";
            like.onclick = false;
            dislike.onclick = false;
            sessionStorage.setItem(question_id, 'liked');
            likeQuestion(like.id);
        };

        dislike.onclick =  function() {
            dislike.style.background = "url(../images/thumbs_down_clicked.png) no-repeat";
            like.style.background = "url(../images/thumbs_up.png) no-repeat";
            dislike.onclick = false;
            like.onclick = false;
            sessionStorage.setItem(question_id, 'disliked');
            dislikeQuestion(dislike.id);
        };

        buttons.appendChild(like);
        buttons.append(dislike);
        card.appendChild(question);

        if(anonimity === 0) {
            getFirstLastName(card, student_id);
        }

        card.appendChild(buttons);
        to_append.appendChild(card);
    }
}

function updateUnderstand() {
    const id = getCookie('user_id');
    if (id === undefined) {
        alert("Log in first");
        window.location.href = "http://localhost:3000/html/home.html";
        return;
    }
    const session = getCookie('token');
    const xhr = new XMLHttpRequest();
    const route = '/API/feedback/understood/' + session + '/' + id;
    xhr.open("PUT", route, true);
    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.onreadystatechange = function() { // Call a function when the state changes.
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            modal2.style.display = "none";
        }
        else if (this.readyState === XMLHttpRequest.DONE && this.status !== 200) {
            window.alert(this.response.toString());
        }
    };

    xhr.send(null);
}

function updateNotunderstand() {
    const id = getCookie('user_id');
    if (id === undefined) {
        alert("Log in first");
        window.location.href = "http://localhost:3000/html/home.html";
        return;
    }
    const session = getCookie('token');
    const xhr = new XMLHttpRequest();
    const route = '/API/feedback/notunderstood/' + session + '/' + id;
    xhr.open("PUT", route, true);
    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.onreadystatechange = function() { // Call a function when the state changes.
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            modal2.style.display = "none";
        }
        else if (this.readyState === XMLHttpRequest.DONE && this.status !== 200) {
            window.alert(this.response.toString());
        }
    };

    xhr.send(null);
}