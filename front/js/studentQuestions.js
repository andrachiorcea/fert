// Get the modal and its corresponding button
const modal = document.getElementById('modal');
const btn = document.getElementsByClassName("generate-token")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
    modal.style.display = "block";
};

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target === modal) {
        modal.style.display = "none";
    }
};

document.addEventListener("DOMContentLoaded", function(){
    getNumberOfLoggedStudents();
});

setTimeout(function(){
    window.location.reload();
}, 300000);

function getNumberOfLoggedStudents() {
    const xhr = new XMLHttpRequest();
    const route = '/API/users/all/' + getCookie('token');
    xhr.open("GET", route , true);
    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.onreadystatechange = function() { // Call a function when the state changes.
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            let present_students = JSON.parse(this.response)[0];
            sessionStorage.setItem('present_students', present_students);
            let M_procent = 0.3;
            let should_appear_to_teacher = M_procent * present_students;
            console.log(should_appear_to_teacher);
            loadQuestions(Math.floor(should_appear_to_teacher));
        }
    };

    xhr.send(null);
}

function loadQuestions(needed_likes) {
    const xhr = new XMLHttpRequest();
    xhr.open("GET", '/API/question/teacher', true);
    xhr.setRequestHeader("Content-Type", "application/json");
    const token = getCookie('token');
    xhr.setRequestHeader("token",token);
    xhr.setRequestHeader("likes",needed_likes);

    xhr.onreadystatechange = function() { // Call a function when the state changes.
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            createQuestionCards(JSON.parse(this.response));
        }
    };

    xhr.send(null);
}

function createQuestionCards(data) {
    let to_append = document.getElementsByClassName('card-container')[0];

    for(let i=0; i<data.length; i++) {
        let question_id = data[i][0];
        let question_content = data[i][1];
        let anonimity = data[i][2];
        let student_id = data[i][3];

        let card = document.createElement('div');
        let question = document.createElement('p');

        card.style.transform = "rotate(" + (Math.random() < 0.5 ? -1 : 1) * Math.random() * 10 + "deg)";
        card.className = 'card';
        card.id = 'card-' + question_id;
        question.className = 'question';
        question.id = 'question-' + question_id;
        question.innerText = question_content;

        if(anonimity === 0) {
            getFirstLastName(card, student_id);
        }

        card.appendChild(question);
        to_append.appendChild(card);
    }
}
