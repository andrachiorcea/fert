const publicVapidKey =
    "BDA2lsxEfak7Pe8mNKfsc7uRVJb8T3cllZbYAj8dKdF8SckOGPSijyFYJTHShD0ZHmBYbn6laOwLxpZ9gGfliHw";

//Check for service worker
if ("serviceWorker" in navigator) {
    send().catch(err => console.error(err));
}
// if user is running mozilla then use it's built-in WebSocket
// window.WebSocket = window.WebSocket || window.MozWebSocket;
//
// var connection = new WebSocket('ws://127.0.0.1:1337');
//
// connection.onopen = function () {
//     // connection is opened and ready to use
// };
//
// connection.onerror = function (error) {
//     // an error occurred when sending/receiving data
// };
//
// connection.onmessage = function (message) {
//     // try to decode json (I assume that each message
//     // from server is json)
//     try {
//         var json = JSON.parse(message.data);
//         console.log(json);
//     } catch (e) {
//         console.log('This doesn\'t look like a valid JSON: ',
//             message.data);
//         return;
//     }
//     // handle incoming message
// };
//
//
// document.getElementById("button-modal").addEventListener("click", function(){
//     var msg = "hello";
//     connection.send(msg);
// });

// Register SW, Register Push, Send Push
async function send() {
    // Register Service Worker
    console.log("Registering service worker...");
    const register = await navigator.serviceWorker.register("/js/worker.js", {
        // scope: "/"
    });
    console.log("Service Worker Registered...");

    // Register Push
    console.log("Registering Push...");
    const subscription = await register.pushManager.subscribe({
        userVisibleOnly: true,
        applicationServerKey: urlBase64ToUint8Array(publicVapidKey),

    });
    console.log("Push Registered...");

    // Send Push Notification
    console.log("Sending Push...");
    await fetch("/subscribe", {
        method: "POST",
        body: JSON.stringify(subscription),
        headers: {
            "content-type": "application/json"
        }
    });
    console.log("Push Sent...");
}

function urlBase64ToUint8Array(base64String) {
    const padding = "=".repeat((4 - base64String.length % 4) % 4);
    const base64 = (base64String + padding)
        .replace(/\-/g, "+")
        .replace(/_/g, "/");

    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);

    for (let i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
}