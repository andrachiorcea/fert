console.log("Service Worker Loaded...");

self.addEventListener("push", e => {
    const data = e.data.json();
    console.log("Push Recieved...");
    self.registration.showNotification(data.title, {
            body: "Please answer the question asked by the teacher",
            icon: "../images/favicon.ico"
        });
});

// self.addEventListener('notificationclick', function(event) {
//     window.open('http://localhost:3000/answerNow.html');
// });

self.addEventListener('notificationclick', function(event) {
    let url = 'http://localhost:3000/answerNow.html';
    event.waitUntil(
        clients.matchAll({type: 'window'}).then( windowClients => {
            // Check if there is already a window/tab open with the target URL
            for (var i = 0; i < windowClients.length; i++) {
                var client = windowClients[i];
                // If so, just focus it.
                if (client.url === url && 'focus' in client) {
                    return client.focus();
                }
            }
            // If not, then open the target URL in a new window/tab.
            if (clients.openWindow) {
                return clients.openWindow(url);
            }
        })
    );
});