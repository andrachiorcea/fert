function getToken(obj) {
    const token = document.getElementById('insert_token').value;
    const xhr = new XMLHttpRequest();
    xhr.open("GET", '/API/token', true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("token",token);

    xhr.onreadystatechange = function() {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            let cookie = "token=" + token + "; max-age=7200;";
            document.cookie = cookie;
            window.location.reload();
            updateSession();
        }
        else if (this.readyState === XMLHttpRequest.DONE && this.status !== 200)  {
            window.alert("Invalid Token");
        }
    };

    xhr.send(null);
}

function getCookie(name) {
    let value = "; " + document.cookie;
    let parts = value.split("; " + name + "=");
    if (parts.length === 2) return parts.pop().split(";").shift();
}

function updateSession() {
    const xhr = new XMLHttpRequest();
    let id = getCookie('user_id');
    if (id === undefined) {
        alert("Log in first");
        window.location.href = "http://localhost:3000/html/home.html";
        return;
    }
    const route = '/API/users/' + id;
    const token = getCookie('token');
    xhr.open("PATCH", route , true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("token",token);

    xhr.onreadystatechange = function() {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            console.log("session is active");
        }
        else if (this.readyState === XMLHttpRequest.DONE && this.status !== 200)  {
            window.alert("Invalid Token");
        }
    };

    xhr.send(null);
}

function getFirstLastName(to_append, student_id) {
    const xhr = new XMLHttpRequest();
    const route = '/API/users/' + student_id;
    xhr.open("GET", route , true);
    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.onreadystatechange = function() {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            return createExtraElements(this.response, to_append);
        }
        else if (this.readyState === XMLHttpRequest.DONE && this.status !== 200)  {
            window.alert("Invalid Token");
        }
    };

    xhr.send(null);
}

function createExtraElements(data, to_append) {
    let name = '';
    if(data !== undefined) {
        for (let i = 0; i < data.length; i++) {
            if (name !== '' && !data[i].match(/[a-z]/i)) {
                name += ' ';
            }
            else if (data[i].match(/[a-z]/i)) {
                name += data[i];
            }
        }
    }
    let div = document.createElement('div');
    div.innerText = name;
    to_append.insertBefore(div, to_append.firstChild);
}

function logout()  {
    document.cookie = 'user_role=; Max-Age=-99999999;';
    document.cookie = 'user_id=; Max-Age=-99999999;';
    document.cookie = 'token=; Max-Age=-99999999;';
    window.location.href = "http://localhost:3000";
    sessionStorage.clear();
}
