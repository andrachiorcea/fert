// window.addEventListener('load', getResponses);
window.addEventListener('load', populatePage);

// Get the modal and its corresponding button
const modal = document.getElementById('modal');
const modal_percentage = document.getElementById('modal1');
modal_percentage.style.display= 'none';
const btn = document.getElementsByClassName("generate-token")[0];
const btn_modal = document.getElementById("button-modal");
const span = document.getElementsByClassName("close")[0];


// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal_percentage.style.display = "none";
};

var answers = [];
var options = [];
var present_students = '';

// When the user clicks the button, open the modal
btn.onclick = function() {
    modal.style.display = "block";
};

btn_modal.onclick = function() {
    modal_percentage.style.display = "block";
};

document.addEventListener("DOMContentLoaded", function(){
    needMoreExplnation();
});

setTimeout(function(){
    window.location.reload();
}, 300000);

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target === modal) {
        modal.style.display = "none";
    }
    else if (event.target === modal_percentage) {
        modal_percentage.style.display = "none";
    }
};

function  populatePage() {
    getResponses();
    getNumberOfLoggedStudentsStats();
    displayStats();
}

function getResponses() {
    const xhr = new XMLHttpRequest();
    xhr.open("GET", '/API/question/teacher/answer', true);
    xhr.setRequestHeader("Content-Type", "application/json");
    const token = getCookie('token');
    const user_id = getCookie('user_id');
    if (user_id === undefined) {
        alert("Log in first");
        window.location.href = "http://localhost:3000/html/home.html";
        return;
    }
    xhr.setRequestHeader("token",token);
    xhr.setRequestHeader("user_id",user_id);

    xhr.onreadystatechange = function() { // Call a function when the state changes.
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            const res = JSON.parse(this.response);
            for(let i=0; i<res.length; i++) {
                answers.push(res[i][0]);
                options.push(res[i][1]);
            }
        }
    };

    xhr.send(null);
}

function getNumberOfLoggedStudentsStats() {
    const xhr = new XMLHttpRequest();
    const route = '/API/users/all/' + getCookie('token');
    xhr.open("GET", route , true);
    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.onreadystatechange = function() { // Call a function when the state changes.
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            present_students = JSON.parse(this.response)[0];
        }
    };

    xhr.send(null);
}

function displayStats() {
    const to_append = document.getElementsByClassName('question-card')[0];

    const xhr = new XMLHttpRequest();
    let id = getCookie('user_id');
    if (id === undefined) {
        alert("Log in first");
        window.location.href = "http://localhost:3000/html/home.html";
        return;
    }
    const route = '/API/question/teacher/' + id;
    xhr.open("GET", route , true);
    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.onreadystatechange = function() { // Call a function when the state changes.
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
           const res = JSON.parse(this.response);
           const question = res[0];
           var valid_options = [];
           res[1] ? valid_options.push(res[1]) : null;
           res[2] ? valid_options.push(res[2]) : null;
           res[3] ? valid_options.push(res[3]) : null;
           res[4] ? valid_options.push(res[4]) : null;

           let questionp = document.createElement('p');
           questionp.className = 'question';
           questionp.innerText = question;
           to_append.appendChild(questionp);

            for(let i=0; i<valid_options.length; i++) {
               let label = document.createElement('label');
               label.id = 'check' + i;
               label.innerText = valid_options[i];

               let span = document.createElement('span');
               span.className = 'option';
               span.id = 'percent' + i;

                for (let j=0; j<options.length; j++) {
                    if (valid_options[i] === options[j]) {
                        let percentage = Math.ceil(answers[j]/present_students * 100);
                        span.innerText = percentage.toString() + "%";
                    }
                }

                if(span.innerText.length === 0) {
                    span.innerText = "0%";
                }

               label.appendChild(span);
               to_append.appendChild(label);
            }
        }
    };

    xhr.send(null);
}

function updatePercentage() {
    let percentage = document.getElementById('percentage').value;
    if (percentage === undefined) {
        alert("You need to fill the field in order to update it");
    }
    else {
        const xhr = new XMLHttpRequest();
        const route = '/API/users/teacher/' + getCookie('user_id');
        xhr.open("PATCH", route, true);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.setRequestHeader("percentage", percentage);

        xhr.onreadystatechange = function () { // Call a function when the state changes.
            if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
                sessionStorage.setItem('percentage', percentage);
                modal_percentage.style.display = 'none';
            }
            else if(this.readyState === XMLHttpRequest.DONE && this.status !== 200) {
                alert ("There has been a problem");
            }
        };

        xhr.send(null);
    }
}