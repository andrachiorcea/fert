var addButton = document.getElementById("add-textarea");

// Get the modal and its corresponding button
const modal = document.getElementById('modal');
const btn = document.getElementsByClassName("generate-token")[0];

// When the user clicks the button, open the modal
btn.onclick = function() {
    modal.style.display = "block";
};

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target === modal) {
        modal.style.display = "none";
    }
};

addButton.onclick = function (event) {
    event.preventDefault();
    const textAreaArray = document.getElementsByClassName("area");
    last_textArea = textAreaArray[textAreaArray.length - 1];
    let index = parseInt(last_textArea.getAttribute('id').substring(4)) + 1;
    if (index < 6) {
        let textArea = document.createElement("textarea");   //create a div
        textArea.className = "area";              //add a class
        textArea.id = "area" + index;
        textArea.placeholder = "Please enter an answer option";
        textArea.maxLength = 100;
        console.log(textArea.getAttribute('id'));
        last_textArea.after(textArea); //append to recommend_section
    }

};

function addQuestion(obj) {
    let question = document.getElementById('area1').value;
    let opt1 = document.getElementById('area2') ? document.getElementById('area2').value : '';
    let opt2 = document.getElementById('area3') ? document.getElementById('area3').value : '';
    let opt3 = document.getElementById('area4') ? document.getElementById('area4').value : '';
    let opt4 = document.getElementById('area5') ? document.getElementById('area5').value : '';
    const id = getCookie('user_id');
    if (id === undefined) {
        alert("Log in first");
        window.location.href = "http://localhost:3000/html/home.html";
        return;
    }
    const session = getCookie('token');
    const data = {};
    data['question'] = question;
    data['teacher_id'] = id;
    data['token'] = session;
    data['opt1'] = opt1;
    data['opt2'] = opt2;
    data['opt3'] = opt3;
    data['opt4'] = opt4;

    const xhr = new XMLHttpRequest();
    xhr.open("POST", '/API/question/teacher', true);
    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.onreadystatechange = function() { // Call a function when the state changes.
        if (this.readyState === XMLHttpRequest.DONE && this.status === 201) {
            window.location.href = "http://localhost:3000/professorHistory.html";
        }
        else if (this.readyState === XMLHttpRequest.DONE && this.status !== 201) {
            window.alert("Server side error");
        }
    };
    xhr.send(JSON.stringify(data));
}