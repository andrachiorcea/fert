function generateToken(obj) {
    const token = document.getElementById('token_value').value;
    let id = getCookie('user_id');
    if (id === undefined) {
        alert("Log in first");
        window.location.href = "http://localhost:3000/html/home.html";
        return;
    }
    const data = {};
    data['token'] = token;
    data['teacher_id'] = id;

    const xhr = new XMLHttpRequest();
    xhr.open("POST", '/API/token/create', true);
    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.onreadystatechange = function() {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 201) {
            modal.style.display = "none";
            console.log('Token created');
            let cookie = "token=" + token + "; max-age=7200;";
            document.cookie = cookie;
        }
        else if (this.readyState === XMLHttpRequest.DONE && this.status !== 201)  {
            window.alert("There was an error on the server side");
        }
    };

    xhr.send(JSON.stringify(data));
}

function needMoreExplnation() {
    const xhr = new XMLHttpRequest();
    const route = "/API/feedback/" + getCookie('token');
    xhr.open("GET", route, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    const percentage = sessionStorage.getItem('percentage');
    const present_students = sessionStorage.getItem('present_students');

    xhr.onreadystatechange = function() { // Call a function when the state changes.
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            let res = JSON.stringify(this.response);
            console.log(res);
            res = res.split('[')[1];
            res = res.split(']')[0];
            console.log(res);
            console.log(percentage);
            console.log(present_students);
            if (res < percentage*present_students) {
                window.alert("The students didn't understand what you are explaining. Please explain again");
            }
        }
    };

    xhr.send(null);
}