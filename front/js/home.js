function register(obj) {
    const registerForm = document.getElementById('register_form');
    const formData = new FormData(registerForm);
    const roles = document.getElementsByName('person_type');
    const data = {};
    let checked = false;

    for (let i = 0, length = roles.length; i < length; i++) {
        if (roles[i].checked) {
            checked = true;
            var person_role = roles[i].value;
            formData.append('person_role', person_role);
        }
    }

    if (!checked) {
        alert('Please select if you are a student or a teacher');
        return;
    }

    formData.forEach((value, key) => {
       data[key] = value;
    });

    const xhr = new XMLHttpRequest();
    xhr.open("POST", '/API/users', true);
    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.onreadystatechange = function() { // Call a function when the state changes.
        if (this.readyState === XMLHttpRequest.DONE && this.status === 201) {
            if (person_role === 'Student') {
                window.location.href = "http://localhost:3000/colleagueQuestions.html";
            }
            else if (person_role === 'Teacher') {
                window.location.href = "http://localhost:3000/studentQuestions.html";
            }
        }
        else if (this.readyState === XMLHttpRequest.DONE && this.status !== 201) {
            window.alert("There has been an error");
        }
    };

    xhr.send(JSON.stringify(data));
}

function login(obj) {
    const loginForm = document.getElementById('login_form');
    const formData = new FormData(loginForm);
    const data = {};

    formData.forEach((value, key) => {
        data[key] = value;
    });

    const xhr = new XMLHttpRequest();
    xhr.open("POST", '/API/users/login', true);
    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.onreadystatechange = function() {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            const user_role = getCookie('user_role');
            if (user_role === 'Student') {
                window.location.href = "http://localhost:3000/colleagueQuestions.html";
            }
            else if (user_role === 'Teacher') {
                window.location.href = "http://localhost:3000/studentQuestions.html";
            }
        }
        else if (this.readyState === XMLHttpRequest.DONE && this.status !== 200)  {
            window.alert("Username or password is incorrect");
        }
    };

    xhr.send(JSON.stringify(data));
}

document.addEventListener('onload', function() {
    const user_id = getCookie('user_id');
    const user_role = getCookie('user_role');
    if(user_id !== undefined && user_role !== undefined) {
        if(user_role === 'Student') {
            window.location.href = "http://localhost:3000/colleagueQuestions.html";
        }
        else {
            window.location.href = "http://localhost:3000/studentQuestions.html";
        }
    }
});
